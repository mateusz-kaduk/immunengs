```@meta
CurrentModule = ImmuneNGS
```

# ImmuneNGS

Documentation for [ImmuneNGS](https://github.com/mateusz-kaduk/immunengs/).

```@autodocs
Modules = [ImmuneNGS]
```

## Index
```@index
```

## Data
```@autodocs
Modules = [Data, Data.IgDiscover]
```

## Stats
```@autodocs
Modules = [Stats]
```

## Types
```@autodocs
Modules = [Types]
```

## Distances
```@autodocs
Modules = [Distances]
```

## Kmers
```@autodocs
Modules = [Kmers]
```

## Motifs
```@autodocs
Modules = [Motifs]
```

## Graphs
```@autodocs
Modules = [Graphs]
```
