using ImmuneNGS
using Documenter

DocMeta.setdocmeta!(ImmuneNGS, :DocTestSetup, :(using ImmuneNGS); recursive=true)

makedocs(;
    modules=[ImmuneNGS],
    authors="Mateusz Kaduk <mateusz.kaduk@ki.se> and contributors",
    repo="https://gitlab.com/mateusz-kaduk/immunengs/blob/{commit}{path}#{line}",
    sitename="ImmuneNGS",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mateusz-kaduk.gitlab.io/immunengs/dev/",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
