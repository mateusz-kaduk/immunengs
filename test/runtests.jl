using ImmuneNGS
using BioSequences
using Test

@testset "ImmuneNGS.jl" begin
    # Test symbol encoding
    @test ImmuneNGS.Kmers.symbol2number('A') == 0
    @test ImmuneNGS.Kmers.symbol2number('C') == 1
    @test ImmuneNGS.Kmers.symbol2number('G') == 2
    @test ImmuneNGS.Kmers.symbol2number('T') == 3
    @test ImmuneNGS.Kmers.number2symbol(0) == 'A'
    @test ImmuneNGS.Kmers.number2symbol(1) == 'C'
    @test ImmuneNGS.Kmers.number2symbol(2) == 'G'
    @test ImmuneNGS.Kmers.number2symbol(3) == 'T'
    # Test kmer encoding
    @test ImmuneNGS.Kmers.pattern2number("ATGC") == 57
    @test ImmuneNGS.Kmers.number2pattern(57, 4) == "ATGC"
    @test length(collect(ImmuneNGS.Kmers.kmer_generator("ATGC",3))) == 2
    # Test distance
    @test ImmuneNGS.Distances.hamming_distance("ATGC","ATCC") == 1
    @test ImmuneNGS.Distances.levenshtein_distance("ATGC","ATCCC") == 2
    @test ImmuneNGS.Distances.longest_common_substring("ATGC","NAATGCCN") == "ATGC"
    @test Set(ImmuneNGS.Distances.immediete_neighbors("AT")) == Set(["AC","CT","AA","AG","GT","TT"])
    @test length(ImmuneNGS.Distances.immediete_neighbors("ATG")) == 9
    @test length(ImmuneNGS.Distances.neighbors("ATG",1)) == 10
    # Test stats
    @test ImmuneNGS.Stats.max_frequency([1,1,2,3,2,2]) == 0.5
    # Test data
    motifs = ["TCGGGGGTTTTT",
              "CCGGTGACTTAC",
              "ACGGGGATTTTC",
              "TTGGGGACTTTT",
              "AAGGGGACTTCC",
              "TTGGGGACTTCC",
              "TCGGGGATTCAT",
              "TCGGGGATTCCT",
              "TAGGGGAACTAC",
              "TCGGGTATAACC"]
    # Test functions operating on motifs
    counts = [2 2 0 0 0 0 9 1 1 1 3 0;
              1 6 0 0 0 0 0 4 1 2 4 6;
              0 0 10 10 9 9 1 0 0 0 0 0;
              7 2 0 0 1 1 0 5 8 7 3 4]
    @test ImmuneNGS.Motifs.counts(motifs) == counts
    profile = (counts ./ 10)
    @test ImmuneNGS.Motifs.profile(counts) == profile
    @test ImmuneNGS.Motifs.consensus(profile) == "TCGGGGATTTCC"
    @test ImmuneNGS.Motifs.score(motifs) == 30
    @test round(ImmuneNGS.Motifs.seq_prob("TTGGTGATTCCT", profile), digits=4) == 0.0001
    @test ImmuneNGS.Motifs.profile_most_probable_kmer("TTGGTGATTCCT", 6, profile) == "TTGGTG"
    best_motifs = ["GGGGGT",
                   "CGGTGA",
                   "CGGGGA",
                   "TGGGGA",
                   "AGGGGA",
                   "TGGGGA",
                   "CGGGGA",
                   "CGGGGA",
                   "AGGGGA",
                   "CGGGTA"]
    @test ImmuneNGS.Motifs.greedy_motif_search(motifs, 6) == best_motifs
    @test round(sum(ImmuneNGS.Motifs.entropy(profile)), digits=1) == 9.9
    # Test randomized motif search using entropy
    random_motifs = ImmuneNGS.Motifs.randomized_motif_search(motifs, 6, 100)[2]
    @test sum(ImmuneNGS.Motifs.entropy(ImmuneNGS.Motifs.profile(ImmuneNGS.Motifs.counts(random_motifs)))) < 3
    # Test trie
    patterns = ["CAGGTGCAGCTGGTGCAGTTTGGG",
                "CAGGTGCAGCTGGTGCAGATTGGG",
                "AGGTGCAGCTGGTGCAGATTGGG",
                "AGGTGCAGCTGGTGCAGATTGGG"]
    g, edge_labels, edge_counts = ImmuneNGS.Graphs.trieConstruction(patterns)
    @test ImmuneNGS.Graphs.prefixTrieMatching("CAGGTGCAGCTGGTGCAGTTTGGG",g, edge_labels, edge_counts) == ("CAGGTGCAGCTGGTGCAGTTTGGG", Set([1]))
    @test ImmuneNGS.Graphs.prefixTrieMatching("CAGA",g, edge_labels,edge_counts) == ("CAGA", Set([4, 2, 3]))
    @test ImmuneNGS.Graphs.prefixTrieMatching("TTGG",g, edge_labels,edge_counts) == ("TTGG", Set([4, 2, 3, 1]))
    @test ImmuneNGS.Graphs.prefixTrieMatching("AGTTTG",g, edge_labels,edge_counts) == ("AGTTTG", Set([1]))
end
