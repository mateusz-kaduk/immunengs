# ImmuneNGS

[![pipeline status](https://gitlab.com/mateusz-kaduk/immunengs/badges/master/pipeline.svg)](https://gitlab.com/mateusz-kaduk/immunengs/commits/master)
[![coverage report](https://gitlab.com/mateusz-kaduk/immunengs/badges/master/coverage.svg)](https://gitlab.com/mateusz-kaduk/immunengs/commits/master)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://mateusz-kaduk.gitlab.io/immunengs/dev/)

ImmuneNGS package consists of selected algorithms useful for working with NGS data of immune receptors.
