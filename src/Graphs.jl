module Graphs
	using Graphs
	using BioSymbols
	using BioSequences
	using GLMakie
	using GraphMakie
	using GraphMakie.NetworkLayout
	"""
	Iterative breath first algorithm
	Tree [1[2[4,5]3[6,7]]] gives [1,2,3,6,7,4,5] traversal
	"""
	function bfs(f, g, root)
		visited = []
		queue = []
		push!(visited, root)
		push!(queue, root)

		while length(queue) != 0
			s = pop!(queue)
			near_neighour = []
			for neighbour in outneighbors(g, s)
				if !(neighbour in visited)
					push!(visited, neighbour)
					push!(queue, neighbour)
					push!(near_neighour, neighbour)
				end
			end
			# Consume near_neighour
			f((s, near_neighour))
		end
		return visited
	end

	"""
	Iterative depth first algorithm
	Tree [1[2[4,5]3[6,7]]] gives [1,3,7,6,2,5,4] traversal
	"""
	function dfs(f, g, root)
		visited = zeros(length(vertices(g)))
		traversal = []
		q = []
		push!(q, root)
		visited[root] = true
		path = []
		while length(q) != 0
			u = pop!(q)
			push!(traversal, u)
			near_neighour = outneighbors(g, u)
			# consume
			f((u, near_neighour))
			for v in near_neighour
				if visited[v] == 0
					push!(q,v)
					visited[v] = true
				end
			end
		end
		return traversal
	end

	"""
	Function to generate all suffixes of a string
	"""
	function suffix_patterns(seq)
	    suffix = Vector{String}()
	    reverse = length(seq):length(seq)
	    while first(reverse) > 0
	        push!(suffix, seq[reverse])
	        reverse = first(reverse)-1:last(reverse)
	    end
	    return suffix
	end

	"""
	Iteratively adds a each string from patterns to the suffix tree
	Returns `graph, edge_labels, edge_counts`
	Where `edge_labels` contain characters and `edge_counts` contains set of lines for which so far traversed pattern occurs
	"""
	function trieConstruction(patterns)
	    root = 1
	    g = DiGraph(root)
	    edge_labels = Dict{Tuple{Int,Int}, Char}()
		edge_counts = Dict{Tuple{Int,Int}, Set}()
		for (n,pattern) in enumerate(patterns)
			suffixes = suffix_patterns(pattern)
		    for seq in suffixes
		        currentNode = root
		        for i in 1:length(seq)
		            currentSymbol = seq[i]
		            endNode = currentNode
		            for nextNode in outneighbors(g, currentNode)
		                if edge_labels[(currentNode, nextNode)] == currentSymbol
		                    endNode = nextNode
							push!(edge_counts[(currentNode, nextNode)], n)
							break
		                end
		            end
		            if endNode != currentNode
		                currentNode = endNode
		            else
		                add_vertex!(g)
		                newNode = nv(g)
		                add_edge!(g, currentNode, newNode)
		                edge_labels[(currentNode, newNode)] = currentSymbol
						edge_counts[(currentNode, newNode)] = Set([n])
						currentNode = newNode
		            end
		        end
		    end
		end
	    return g, edge_labels, edge_counts
	end

	"""
	Use GraphMakie to plot the graph with root node market in red and Buchheim layout for trees
	Optional parameter `edge_labels=` when specificed prints characters on the edges.
	"""
	function triePlot(g; edge_labels=Dict{Tuple{Int,Int}, Char}())
	    colors = [:black for i in 1:nv(g)]
	    colors[1] = :red
	    if length(edge_labels) == 0
	        return graphplot(g,layout=Buchheim(),node_color=colors)
	    end
	    labels = Vector{String}()
	    for edge in edges(g)
	        dst = edge.dst
	        src = edge.src
	        push!(labels, repr.(convert(Char, edge_labels[src,dst])))
	    end
	    graphplot(g,elabels=labels, layout=Buchheim(), node_color=colors, elabels_color=[:black for i in 1:ne(g)], edge_color=[:black for i in 1:ne(g)])
	end

	"""
	PrefixTrieMatching algorithm finds whether text matches any patterns in trie.
	Returns `match` which is a matching string and `set` of lines in which pattern occurs.
	"""
	function prefixTrieMatching(text::String, g::DiGraph, edge_labels::Dict{Tuple{Int,Int}, Char}, edge_counts::Dict{Tuple{Int,Int}, Set})
	    i = 1
	    root = 1
	    symbol = text[i]
	    v = root
	    match = []
		nmatch = Set()
	    while true
	        w = 0
	        for o in outneighbors(g, v)
	            if has_edge(g,v,o) & (edge_labels[(v,o)] == symbol)
	                w = o
	            end
	        end
	        if (w > 0) && (i == length(text))
	            push!(match, text[i])
				nmatch = edge_counts[(v,w)]
				break
	        elseif (w != 0) && (i < length(text))
				push!(match, text[i])
	            i += 1
	            symbol = text[i]
	            v = w
	        else
	            match = []
	            break
	        end
	    end
	    return join(match), nmatch
	end
end
