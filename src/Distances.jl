module Distances
    export hamming_distance
    export levenshtein_distance
    export longest_common_substring

    """
    Compute Hamming distance
    """
    function hamming_distance(s1, s2)
        @assert length(s1) == length(s2)
        return sum([x != y for (x,y) in zip(s1, s2)])
    end

    """
    Compute Levenshtein distance
    """
    function levenshtein_distance(s1, s2)
		lens1 = length(s1)+1
		lens2 = length(s2)+1
		d = zeros(lens1,lens2)
		
		for i in 2:lens1
			d[i, 1] = i-1
		end
		
		for j in 2:lens2
			d[1,j] = j-1
		end
		
		for j in 2:lens2
			for i in 2:lens1
				if s1[i-1] == s2[j-1]
					d[i,j] = d[i-1,j-1]
				else
					d[i,j] = minimum((d[i-1,j]+1, d[i,j-1]+1, d[i-1,j-1]+1))
				end	
			end
		end
		d[lens1,lens2]
    end

    """
    Compute longest common substring between x and y, using Dynamic programming approach

        - Naive LCS is O(n*m)^2
        - Dynamic programming LCS (this) is O(m*n)
        - Suffix tree LCS is O(n+m) linear, but for this application with short strings DP is sufficient

    Return longest common substring between two strings
    """
    function longest_common_substring(x::String, y::String)
    	m = length(x)
    	n = length(y)

    	lc = zeros(m,n)
    	result = 0
    	maxi,maxj = 0,0
    	for i in 1:m
    		for j in 1:n
    			if (i == 1) | (j == 1)
    				lc[i,j] = 0
    			elseif x[i-1] == y[j-1]
    				lc[i,j] = lc[i-1,j-1]+1
    				maxres = max(result, lc[i,j])
    				if maxres > result
    					maxi,maxj = i,j
    				end
    				result = maxres
    			else
    				lc[i,j] = 0
    			end
    		end
    	end

    	# Backtrace
    	rseq = []
    	i,j = maxi+1,maxj+1
    	while (i > (maxi-result)) & ((j > (maxj-result)))
    		i -= 1
    		j -= 1
    		push!(rseq,x[i])
    	end
    	return join(reverse(rseq))
    end

    """
    Generate all immediate neigbors, meaning all patterns 1 distance away
    """
    function immediete_neighbors(pattern)
    	neigborhood = []
    	for i in 1:length(pattern)
    		symbol = pattern[i]
    		for nucleotide in "ATGC"
    			if nucleotide != symbol
    				neighbor = pattern[begin:i-1]*nucleotide*pattern[i+1:end]
    				push!(neigborhood, neighbor)
    			end
    		end
    	end
    	return neigborhood
    end

    """
    Generate all patterns at most d distance form staring pattern
    """
    function neighbors(pattern, d)
    	if d == 0
    		return [pattern]
    	end
    	if length(pattern) == 1
    		return ["A","T","G","C"]
    	end
    	neighborhood = []
    	suffixneighbors = neighbors(pattern[2:end], d)
    	for neigbor in suffixneighbors
    		if hamming_distance(pattern[2:end], neigbor) < d
    			for nucleotide in "ATGC"
    				push!(neighborhood,nucleotide*neigbor)
    			end
    		else
    				push!(neighborhood,pattern[1]*neigbor)
    		end
    	end
    	return neighborhood
    end
end
