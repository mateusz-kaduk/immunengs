module Kmers
    include("Types.jl")
    using .Types
    export kmer_generator, index_kmers
    export pattern2number, number2pattern
    using ProgressMeter

    """
    Symbol to number lookup function
    """
    function symbol2number(symbol)
        return dna2ind[symbol]-1
    end

    """
    Number to symbol lookup function
    """
    function number2symbol(number)
        return ind2dna[number+1]
    end

    """
    Recursively transform pattern into number
    """
    function pattern2number(pattern)
        if length(pattern) == 0
            return 0
        end
        return 4*pattern2number(pattern[begin:end-1]) + symbol2number(pattern[end])
    end

    """
    Recursively transform number into kmer of length k
    """
    function number2pattern(index, k)
        if k == 1
            return number2symbol(index)
        end
        prefixIndex = div(index, 4)
        r = rem(index, 4)
        symbol = number2symbol(r)
        prefixpattern = number2pattern(prefixIndex, k-1)
        return prefixpattern*symbol
    end

    """
    Generator extracting kmers of specific length from a sequence
    """
    function kmer_generator(seq, k)
    	return (seq[i:i+k-1] for i in 1:length(seq)-k+1)
    end

    function counter(vector::AbstractVector)
    	"""
    	Simple counter using sorting
    	"""
    	# Handle special case
    	if length(vector) == 1
    		return [(vector[end],1)]
    	end
    	vec = sort(vector)
    	c = 1
    	counts = []
    	for i in 1:length(vec)-1
    		if vec[i] == vec[i+1]
    			c += 1
    		else
    			push!(counts, (vec[i],c))
    			c = 1
    		end
    		if (i == (length(vec)-1))
    			push!(counts, (vec[i+1],c))
    		end
    	end
    	return counts
    end

    """
    Build an array with indices for each kmer occurence
    """
    function index_kmers(sequences::Vector{String}; k=9)
        kmer_indices = Dict{String, AbstractVector}()
        @showprogress for i in 1:length(sequences)
            # Following kmers belong to index i
            for kmer in kmer_generator(sequences[i],k)
                Types.append!(kmer_indices, kmer, i)
            end
        end
        return kmer_indices
    end
end
