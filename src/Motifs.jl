module Motifs
	include("Data.jl")
	include("Kmers.jl")
	include("Types.jl")
	include("Distances.jl")
	using .Data: pad_sequences
	using .Kmers
	using .Types
	using .Distances
	using SparseArrays: SparseMatrixCSC, nonzeros
	using Random: rand, shuffle

	export gibbs_sampler

	"""
	Compute counts given motifs of equal length
	"""
	function counts(motifs)
	    cols = length(motifs[1])
	    rows = length(motifs)
	    c = zeros(Int64, length(dna2ind), cols)
	    for col in 1:cols
	        for row in 1:rows
	            ind = get(dna2ind,motifs[row][col],0)
	            if ind > 0  # Gaps have zero count and probability
	                c[ind,col] += 1
	            end
	        end
	    end
	    return c
	end

	"""
	Compute profile from counts
	"""
	function profile(c)
	    return c./sum(c,dims=1)
	end

	"""
	Compute consensus sequence from a profile
	"""
	function consensus(p)
	    return join([ind2dna[argmax(p[:,i])] for i in 1:size(p)[2]])
	end

	"""
	Score motifs by summing up distance of every motif to their consensus
	"""
	function score(motifs)
	    p = profile(counts(motifs))
	    c = consensus(p)
	    dist = 0
	    for i in 1:length(motifs)
	        dist += hamming_distance(c, motifs[i])
	    end
	    return dist
	end

	"""
	Calcualte Shannon entropy for each column of a profile.
	Zeros remain zeros.
	"""
	function entropy(p)
	    sp =  SparseMatrixCSC(p) # Use sparse matrice to skip computing log2 of zero, instead keep zero
	    n = nonzeros(sp)
	    n[:] = log2.(n)
	    return -sum(sp .* p,dims=1)
	end

	"""
	Calculate the product of all probabilties from nucleotides in a sequence given profile
	Returns probability of a sequence
	"""
	function seq_prob(s, p)
	    accu = 0.0
	    for i in 1:length(s)
	        if s[i] == '-'
	            return 0 # Sequences with gaps are not possible
	        end
	        ind = get(dna2ind, s[i], 0)
	        if ind > 0
	            accu += log2(p[ind, i])
	        end
	    end
	    return 2^(accu) # Summing of logs more numerically stable
	end

	"""
	Find most probable kmer in a text given kmers profile
	"""
	function profile_most_probable_kmer(text, k, p)
	    most = -1
	    most_kmer = ""
	    for kmer in kmer_generator(text, k)
	        prob = seq_prob(kmer, p)
	        if prob > most
	            most = prob
				most_kmer = kmer
	        end
	    end
	    return most_kmer
	end

	"""
	Greedy algorithm for motif finding across multiple dna sequences
	"""
	function greedy_motif_search(dna, k)
		best_score = [Inf, []]
		for kmer in kmer_generator(dna[1], k)
			motifs = [kmer]

			# Find the most probable kmer in next string
			# and update profile for each string
			for j in 2:length(dna)
				current_profile = profile(counts(motifs) .+ 1) # Laplace's smoothing
				most_probable = profile_most_probable_kmer(dna[j], k, current_profile)
				push!(motifs, most_probable)
			end

			# Check if new best score list of motifs
			current_score = score(motifs)
			if current_score < best_score[1]
				best_score = [current_score, motifs]
			end
		end
		return best_score[end]
	end

	"""
	Generate a list of most probable motifs from profile given dna and k-length
	"""
	function motifs_from_profile(profile, dna::Vector{String}, k::Int)
		return [profile_most_probable_kmer(seq, k, profile) for seq in dna]
	end

	"""
	An instance of single randomized motif search
	"""
	function randomized_motif_search_single(seqs::AbstractVector{String}, k::Int)
		@assert minimum(length.(seqs)) >= k
		rand_ints = [rand(1:length(seqs[1])-k+1) for a in 1:length(seqs)]
		motifs = [seqs[i][r:r+k-1] for (i,r) in enumerate(rand_ints)]

		best_score = [score(motifs), motifs]

		while true
			current_profile = profile(counts(motifs) .+ 1)
			motifs = motifs_from_profile(current_profile, seqs, k)
			current_score = score(motifs)
			if current_score < best_score[1]
				best_score = [current_score, motifs]
			else
				return best_score
			end
		end
	end

	"""
	Randomized motif search algorithm

		- seqs Vector of sequences
		- k Length of a pattern
		- n Number of iterations

	Return representative patterns
	"""
	function randomized_motif_search(seqs::AbstractVector{String}, k::Int, n::Int)
		best_motifs = [k*length(seqs[1]), []]

		for repeat in 1:n
			current_motifs = randomized_motif_search_single(seqs, k)
			if current_motifs[1] < best_motifs[1]
				best_motifs = current_motifs
			end
		end
		return best_motifs
	end

	"""
	Gibbs sampling motif search algorithm
	"""
	function gibbs_sampler_single(seqs::AbstractVector{String}, k::Int, n::Int)
		t=length(seqs)
		rand_ints = [rand(1:length(seqs[a])-k) for a in 1:t]
		motifs = [seqs[i][r:r+k-1] for (i,r) in enumerate(rand_ints)]
		best_motifs = copy(motifs)

		for j in 1:n
			r = rand(1:t)
			current_profile = profile(counts([motif for (index, motif) in enumerate(motifs) if index != r]) .+ 1)
			motifs[r] = profile_most_probable_kmer(seqs[r],k,current_profile)
			if score(motifs) < score(best_motifs)
				best_motifs = copy(motifs)
			end
		end
		return best_motifs
	end

	"""
	Gibbs sampling motif search algorithm

		- seqs Vector of sequences
		- k Length of a pattern
		- n Number of iterations
		- r Number of random initializations

	Return most representative patterns
	"""
	function gibbs_sampler(motifs::AbstractVector{String}, k::Int,n::Int; r=1)
		best_motifs = copy(motifs)
		for i in 1:r
			current_motifs = gibbs_sampler_single(motifs, k, n)
			if score(current_motifs) < score(best_motifs)
				best_motifs = copy(current_motifs)
			end
		end
		return best_motifs
	end

	"""
	Uses gibbs sampling to discover representative core for starting pattern

		sequences - Vector of sequences to search through
		pattern - Usually most frequent pattern to start extension from
		range - Range of allowed ranges for core pattern
		n - Number of seqences to sample (reduces computatoinal cost)
	"""
	function discover_core(sequences::AbstractVector{String}, pattern::String; range=9:50, n=500, r=3, ratio=0.2, mincount=100)
		motifs = sequences[occursin.((pattern), sequences)]
		n = minimum([n,length(motifs)])
		motifs = motifs[shuffle(1:n)]  # Only take at most n samples
		padded_motifs = pad_sequences(motifs)
		prev_cons_count = 1
		best = [(length(pattern),pattern,profile(counts(padded_motifs)))]
		min = minimum(range)
		max = minimum([maximum(range),length(padded_motifs[1])])  # We can't go more than the longest sequence
		for k in min:max
			sampled_counts = counts(gibbs_sampler(padded_motifs, k, n, r=r))
			cons = consensus(sampled_counts)
			cons_count = sum(occursin.((cons), sequences))
			if (cons_count / prev_cons_count) < ratio
				break
			end
			if cons_count < mincount
				break
			end
			prev_cons_count = copy(cons_count)
			push!(best,(k,cons,profile(sampled_counts)))
		end
		return best[end]
	end
end  # module
