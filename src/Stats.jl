module Stats
    include("Kmers.jl")
    using .Kmers: counter
    export max_frequency
    """
    Calculate the frequency for the item with the maximum number of occurences
    """
    function max_frequency(items::AbstractVector)
        count = map(c->c[2], counter(items))
        return maximum(count)/sum(count)
    end
end
