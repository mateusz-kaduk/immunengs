module Types

export dna2ind
export ind2dna

const dna2ind = Dict{Char,Int}('A'=>1,'C'=>2,'G'=>3,'T'=>4)
const ind2dna = Dict{Int,Char}(1=>'A',2=>'C',3=>'G',4=>'T')

"""
Append vector in Dict{String, AbstractVector{T} where T} at given key.
If key does not exist, create one.
"""
function append!(d::Dict{S, V}, k::S, i::I) where {S<:String, I<:Integer, V<:AbstractVector}
    if haskey(d, k)
        if !any(i .== d[k])  # Add indices only once
            push!(d[k], i)
        end
    else
        d[k] = [i]
    end
end

end
