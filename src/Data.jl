module Data
    using CSV: File
    using DataFrames: DataFrame
    using FASTX

    """
    Load records from FASTA file
    """
    function read_fasta(path)
        records = []
        open(FASTA.Reader, path) do reader
            for record in reader
                push!(records, (FASTX.identifier(record), FASTX.sequence(record)))
            end
        end
        return records
    end

    """
    Read table from a TSV file

    Return DataFrame
    """
    function read_table(path::String)
        return File(path, delim='\t') |> DataFrame
    end

    """
    Function to pad sequences up to the length of longest sequence
    """
    function pad_sequences(seqs)
    	maxlen = maximum([length(seqs[i]) for i in 1:length(seqs)])
    	padded_seqs = Vector{String}()
    	for seq in seqs
    		if length(seq) < maxlen
    			padding = repeat('-',maxlen-length(seq))
    			seq *= padding
    		end
    		push!(padded_seqs, seq)
    	end
    	return padded_seqs
    end

    """
    Sub-module handling IgDiscover assignment table
    """
    module IgDiscover
        """
        Create a :D_segment column, spaning region
        between last nucleotide of V and first nucleotide of J
        """
        function create_dsegments!(table)
        	dsegments = []
        	for row in eachrow(table)
        		start = maximum(findfirst(row[:V_nt],row[:genomic_sequence]))
        		stop = minimum(findfirst(row[:J_nt],row[:genomic_sequence]))
        		dsegment = row[:genomic_sequence][start:stop]
        		push!(dsegments,dsegment)
        	end
        	table[:,:D_segment] = dsegments
        	return table
        end
    end
end
