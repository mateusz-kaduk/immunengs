module ImmuneNGS

include("Data.jl")
include("Kmers.jl")
include("Motifs.jl")
include("Graphs.jl")
include("Distances.jl")
include("Types.jl")
include("Stats.jl")

using .Motifs: gibbs_sampler, counts, consensus, discover_core
using .Data: read_table, pad_sequences
using .Stats: max_frequency
using ArgParse
using ProgressMeter
using DataFrames
using CSV

function parse_commandline()
    s = ArgParseSettings()
    @add_arg_table! s begin
        "assignments"
            help = "IgBLAST assignments table from IgDiscover"
            required = true
        "output"
            help = "TSV file to store results"
            required = true
        "-c", "--mincount"
            help = "Minimum kmer count allowed"
            default = 100
            arg_type = Int
            range_tester = (x->x >= 1)
        "-l", "--minlength"
            help = "Minimum candidate length allowed"
            default = 100
            arg_type = Int
            range_tester = (x->x >= 1)
        "-k", "--kmer"
            help = "k-mer length"
            default = 7
            arg_type = Int
            range_tester = (x->x >= 1)
        "-i", "--iters"
            help = "Iterations for Gibbs sampler"
            default = 200
            arg_type = Int
            range_tester = (x->x >= 1)
        "-r", "--random"
            help = "Random initializations for Gibbs sampler"
            default = 200
            arg_type = Int
            range_tester = (x->x >= 1)

    end
    return parse_args(s)
end
#"-j", "--json"
#help = "JSON file with dictionary containing haptamers"
#default = "heptamers.json"

#"-r","--ratio"
#help = "Lowest allowed ratio between counts of full allele sequence and trimmed allele sequence"
#default = 0.75
#arg_type = Float64
#range_tester = (x-> (x >= 0.0) & (x <= 1.0))
#end

function traverse(core, graph, edge_labels, edge_counts; list=[], side=true, path=[])
    step = []
    parent_match, parent_lines = ImmuneNGS.Graphs.prefixTrieMatching(core, graph, edge_labels, edge_counts)
    push!(path, step)
    # Try every direction
    for c in "ATGC"
        local param
        if side
            param = core*c
        else
            param = c*core
        end
        match, lines = ImmuneNGS.Graphs.prefixTrieMatching(param, graph, edge_labels, edge_counts)
        push!(step, (match, length(lines)))
    end
    # Stop condition
    if length([t for t in step if t[2]/length(parent_lines) > 0.15]) >= 3
        return list, path
    end
    # Select only relevant paths for the next step and traverse deeper
    list = [t for t in step if t[2]/length(parent_lines) > 0.25]
    for elem in list
        # Window
        #if length(path) >= 10
        #    popfirst!(path)
        #end
        list = traverse(elem[1], graph, edge_labels, edge_counts, list=list, side=~side, path=path)
    end
    # Return path
    return list
end

function kmer_candidates(core, samples, graph, edge_labels, edge_counts)
    side = true
    candidates = [[(core, length(samples))]]
    for (i,step) in enumerate(candidates)
        # Switch side
        side = !side
        for (j,(candidate,count)) in enumerate(step)
            local param
            step = []
            # Extend in one direction four nucleotides
            for c in "ATGC"
                if side
                    param = candidate*c
                else
                    param = c*candidate
                end
                match, lines = ImmuneNGS.Graphs.prefixTrieMatching(param, graph, edge_labels, edge_counts)
                push!(step, (match, length(lines)))
            end
            # Skip empty steps if no matches on the suffix tree
            if length(step) == 0
                continue
            end
            # Update current candidates at this step
            prev_candidates = candidates[1]
            total = sum([count for (candidate,count) in prev_candidates])
            current_total = sum([count for (candidate, count) in step])
            current_max = maximum([count for (candidate, count) in step])
            #for (candidate, count) in step
            #    @info("Step $i, split $j, forward $side -> $candidate with $count and $(count/total) frequency and ratio of $(count/current_max)")
            #end
            # Which candidates to keep
            update = [(candidate,count) for (candidate,count) in step if (count / total) > 0.15]
            check = [(candidate,count) for (candidate,count) in step if (count / current_max) > 0.3]
            if length(check) > 2
                # Fuzzy region, do not update but continue
                #@info("Fuzzy")
                continue
            end
            if length(update) > 0
                push!(candidates, update)
            end
        end
    end
    return candidates
end

function collapse(candidates)
    singleton = []
    for (candidate,count) in vcat(candidates...)
        found = 0
        for (seq,n) in vcat(candidates...)
            if occursin(candidate, seq)
                found += 1
            end
        end
        if found <= 1
            push!(singleton, (candidate, count))
        end
    end
    return singleton
end


function main()
    parsed_args = parse_commandline()
    table = ImmuneNGS.Data.read_table(parsed_args["assignments"])
    @info "Loaded $(nrow(table)) reads"
    table = filter(r -> !ismissing(r.CDR3_nt), table)
    @info "After discarding missing CDR3_nt $(nrow(table)) reads"
    dropmissing!(table, :D_region)
    @info "After discarding missing D_region $(nrow(table)) reads"
    ImmuneNGS.Data.IgDiscover.create_dsegments!(table)

    # Iterate kmers and lines where they occur to compute stats
    kmers = ImmuneNGS.Kmers.index_kmers(String.(table[:,:D_segment]), k=parsed_args["kmer"])
    rows = []
    for (i,(k,v)) in enumerate(kmers)
        most_frequent_count = ImmuneNGS.Kmers.counter(length.(table[v,:CDR3_nt]))[1][2]
        most_frequent_CDR3 = most_frequent_count/length(v)
        push!(rows, (k, length(v), most_frequent_CDR3))
    end

    p = Progress(length(core_kmers))
    results = Folds.map(core_kmers) do row
        core, count, ratio = row
        samples = table[kmers[core],:D_segment]
        # Build more managable graph
        graph, edge_labels, edge_counts = ImmuneNGS.Graphs.trieConstruction(samples)
        # Extensions
        extended = ImmuneNGS.collapse(ImmuneNGS.kmer_candidates(core, samples,graph, edge_labels, edge_counts))
        extended = [(core,count) for (core,count) in extended if length(core) >= parsed_args["minlength"]]
        # Gibbs sampler
        gibbs = ImmuneNGS.Motifs.discover_core(String.(samples), core, r=parsed_args["random"], n=parsed_args["iters"], mincount=parsed_args["mincount"])
        # Update progress
        next!(p)
        [(core, extension, count, gibbs[2]) for (extension, count) in extended]
    end
    results_df = DataFrame(vcat([r for r in results if length(r) > 0]...))
    
    rename!(results_df, ["kmer","extension","extension_count","gibbs_consensus"])
    CSV.write(parsed_args["output"], results_df, delim='\t')
end

function julia_main()::Cint
    main()
    return 0
end

end
